<?php require_once "conexion.php"; ?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function() {
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Poblacion estudiantil"
	},
	axisY: {
		title: "Estudiantes por carrera"
	},
	data: [{
		type: "column",
		yValueFormatString: "#### estudiantes",
		dataPoints: <?=json_encode($datosb, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>