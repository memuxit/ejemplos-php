<?php
// Estableciendo conexion a la base de datos
$conexion = new mysqli("localhost", "root", "", "patrones");
// Realizando un select completo de la tabla poblacion
$query = "SELECT * FROM poblacion";
// Ejecutando query y guardando resultset en la variable resultado
$resultado = $conexion->query($query);
// Creando arreglo y agregandole cabeceras (para Google Chart)
$datos = array(array("Carrera", "Poblacion"));
// Creando arreglo (para CanvasJS)
$datosb = array();
// Recorriendo resultset y obteniendo objetos del mismo
while ($registro = $resultado->fetch_object()) {
    // Agregando arreglo de objetos al arreglo (Google Chart)
    array_push($datos, array($registro->carrera, (int) $registro->cantidad));
    // Agregando arreglo de objetos al arreglo (CanvasJS)
    array_push($datosb, array("y" => (int) $registro->cantidad, "label" => $registro->carrera));
}
// Cerrando conexion
$conexion->close();