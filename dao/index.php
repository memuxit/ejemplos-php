<?php require_once "model/Producto.php";
session_start(); ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Demo con Dao</title>
</head>
<body>
    <h1>Ingreso de productos</h1>
    <form action="php/procesar.php" method="post">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre" required autofocus>
        <label for="marca">Marca:</label>
        <input type="text" name="marca" id="marca" required>
        <input type="submit" value="Agregar">
    </form>
    <h1>Listado de productos</h1>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Marca</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // Evaluando si existe variable de sesion
            if (isset($_SESSION["productos"])) {
                // Recorriendo el arreglo
                foreach ($_SESSION["productos"] as $producto) { ?>
                    <tr>
                        <td><?=$producto->id; ?></td>
                        <td><?=$producto->nombre; ?></td>
                        <td><?=$producto->marca; ?></td>
                    </tr>
                <?php }
                session_destroy();
            }
            ?>
        </tbody>
    </table>
</body>
</html>