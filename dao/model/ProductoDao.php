<?php

 require_once "../core/DataSource.php";

 require_once "IDao.php";

 require_once "Producto.php";

 /**
  * Clase que implementa los métodos CRUD para el DTO Producto mediante la interfaz IDao
  *
  * @author Ing. Guillermo Rafael Vásquez Castaneda <email@email.com>
  * @version 1.0
  */
 class ProductoDao implements IDao
 {
     /**
      * Atirbuto de la clase
      *
      * @var PDO
      */
     private $conexion;

     /**
      * Constructor vacío de la clase
      */
     public function __construct()
     {
         $conexion = new DataSource();
         $this->conexion = $conexion->conectar();
     }

     /**
      * Metodo que guarda un nuevo registro del tipo Producto
      *
      * @param Producto objeto del tipo Producto
      * @return void
      */
     public function save($producto)
     {
         $query = "INSERT INTO productos (nombre, marca) VALUES (:nombre, :marca)";
         $stmt = $this->conexion->prepare($query);
         $stmt->execute(array(":nombre" => $producto->nombre, ":marca" => $producto->marca));
     }

     /**
      * Metodo que actualizar un registro de tipo Producto
      *
      * @param Producto objeto del tipo poblacion
      * @return void
      */
     public function update($producto)
     {
         $query = "UPDATE productos SET nombre = :nombre, marca = :marca WHERE id = :id";
         $stmt = $this->conexion->prepare($query);
         $stmt->execute(array(":nombre" => $producto->nombre, ":marca" => $producto->marca, ":id" => $producto->id));
     }

     /**
      * Metodo que elimina un registro del tipo Producto
      *
      * @param int id del Producto
      * @return void
      */
     public function delete($id)
     {
         $query = "DELETE FROM productos WHERE id = :id";
         $stmt = $this->conexion->prepare($query);
         $stmt->execute(array(":id" => $id));
     }

     /**
      * Metodo que devuelve un arreglo con todos los registros del tipo Producto
      *
      * @return array arreglo con objetos del tipo Producto
      */
     public function all()
     {
         $producto = null;
         $productos = array();
         $query = "SELECT * FROM productos";
         $stmt = $this->conexion->prepare($query);
         $stmt->execute();
         while ($registro = $stmt->fetch(PDO::FETCH_OBJ)) {
             $producto = new Producto();
             $producto->id = (int) $registro->id;
             $producto->nombre = (string) $registro->nombre;
             $producto->marca = (string) $registro->marca;
             array_push($productos, $producto);
         }
         return $productos;
     }
 }
 