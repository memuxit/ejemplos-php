<?php
/**
 * Interfaz que contiene los métodos para realizar el CRUD
 * 
 * @author Ing. Guillermo Rafael Vásquez Castaneda <memuxit@gmail.com>
 * @version 1.0
 */
interface IDao
{
    public function save($object);

    public function update($object);

    public function delete($id);
    
    public function all();
}