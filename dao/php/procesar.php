<?php
// Evaluando si existe algo por POST con el name de nombre
if (isset($_POST["nombre"])) {
    // Realizando requires necesarios
    require_once "../model/Producto.php";
    require_once "../model/ProductoDao.php";
    // Creando objeto del DTO y asignando valores a los atributos
    $producto = new Producto();
    $producto->nombre = $_POST["nombre"];
    $producto->marca = $_POST["marca"];
    // Creando objeto del ProductoDao y haciendo uso del metodo save
    $productoDao = new ProductoDao();
    $productoDao->save($producto);
    // Indicando manejo de variables de sesion
    session_start();
    // Asignando arreglo de DTO a la variable de sesion productos
    $_SESSION["productos"] = $productoDao->all();
}
// Redireccionando al index
header("Location:../index.php");