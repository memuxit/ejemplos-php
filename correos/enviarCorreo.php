<?php
// Estos require son necesarios para hacer uso de PHPMailer
require_once 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

//Indica el correo de donde se envia el correo
$emailFrom='juan@gmail.com'; 
// Nombre de la persona que envia el correo
$emailFromName='Juan Perez';
//Indica la direccion de correo a donde se envia el mensaje
$emailTo='pedro@gmail.com';
// Nombre de la persona a la que se le envia el correo
$emailToName='Pedro Páramo';
$mail = new PHPMailer;
$mail->isSMTP(); 
$mail->SMTPDebug = 2; 
// Host que nos otorga Google mediante una cuenta de gmail
$mail->Host = "smtp.gmail.com"; 
$mail->Port = 587; // TLS only y  para SSL: 465
$mail->SMTPSecure = 'tls';  // SSL
$mail->SMTPAuth = true;
//Correo de gmail que se configuro 
$mail->Username = 'juan@gmail.com';
// Contraseña de tu cuenta de correo electrónico configurado
$mail->Password = 'itca123';
$mail->setFrom($emailFrom, $emailFromName);
$mail->addAddress($emailTo, $emailToName);
// Asunto del correo electronico
$mail->Subject = 'Correos con PHPMailer';
// Toda la informacion que se necesita enviar escrita
$mail->msgHTML("Este es un correo de pruebas..."); 
$mail->AltBody = 'Mensaje no soportado';
//se envia el mensaje, si no ha habido problemas
//la variable $exito tendra el valor true
$exito = $mail->Send();
//Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho
//para  intentar  enviar  el  mensaje,  cada  intento  se  hara  5  segundos despues
//del anterior, para ello se usa la funcion sleep
$intentos = 1;
while ((!$exito) && ($intentos < 5)) {
    sleep(5);
    //echo $mail->ErrorInfo;
    $exito = $mail->Send();
    $intentos = $intentos + 1;
}
if(!$exito) {
	echo "Problemas enviando correo electrónico a pedro@gmail.com";
	echo "<br/>".$mail->ErrorInfo;
} else {
	echo "Mensaje enviado correctamente";
}