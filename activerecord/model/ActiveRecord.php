<?php
/**
 * Clase que contiene los metodos base para obtener y eliminar registros
 * 
 * @author Ing. Guillermo Rafael Vásquez Castaneda <memuxit@gmail.com>
 * @version 1.0
 */
class ActiveRecord
{
    /**
     * Atributos de la clase
     *
     * @var object
     */
    protected $datasource;
    protected $table;

    /**
     * Constructor de la clase
     *
     * @param PDO conexion con la base de datos
     * @param string nombre de la tabla
     */
    public function __construct($datasource, $table)
    {
        $this->datasource = $datasource;
        $this->table = $table;
    }

    /**
     * Setter de los atributos
     *
     * @param string nombre del atributo
     * @param object valor para el atributo
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Getter de los atributos
     *
     * @param string nombre del atributo
     * @return object valor del atributo
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Metodo que devuelve todos los registros de una tabla
     *
     * @return array arreglo con objetos que representan registros de la tabla
     */
    public function all()
    {
        $registros = array();
        $query = "SELECT * FROM {$this->table}";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute();
        while ($registro = $stmt->fetch(PDO::FETCH_OBJ)) {
            array_push($registros, $registro);
        }
        return $registros;
    }

    /**
     * Metodo que elimina un registro en base a su id
     *
     * @param int id del registro a eliminar
     * @return void
     */
    public function delete($id)
    {
        $query = "DELETE FROM {$this->table} WHERE id = :id";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":id" => $id));
    }
}