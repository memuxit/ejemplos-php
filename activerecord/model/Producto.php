<?php

require_once "ActiveRecord.php";

/**
 * Clase que representa la tabla productos, ademas hereda las funcionalidades
 * de ActiveRecord
 * 
 * @author Ing. Guillermo Rafael Vásquez Castaneda <memuxit@gmail.com>
 * @version 1.0
 */
class Producto extends ActiveRecord
{
    /**
     * Atributos de la clase
     *
     * @var object
     */
    private $id;
    private $nombre;
    private $marca;

    /**
     * Constructor de la clase que a su vez, llama al constructor padre
     *
     * @param PDO conexion con la base de datos
     */
    public function __construct($datasource)
    {
        parent::__construct($datasource, "productos");
    }

    /**
     * Setter de los atributos
     *
     * @param string nombre del atributo
     * @param object valor para el atributo
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Getter de los atributos
     *
     * @param string nombre del atributo
     * @return object valor del atributo
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Metodo que guarda un nuevo registro
     *
     * @return void
     */
    public function save()
    {
        $query = "INSERT INTO productos (nombre, marca) VALUES (:nombre, :marca)";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":nombre" => $this->nombre, ":marca" => $this->marca));
    }

    /**
     * Metodo que actualiza un registro
     *
     * @return void
     */
    public function update()
    {
        $query = "UPDATE productos SET nombre = :nombre, marca = :marca WHERE id = :id";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":nombre" => $this->nombre, ":marca" => $this->marca, ":id" => $this->id));
    }
}
