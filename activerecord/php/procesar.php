<?php
// Evaluando si existe algo por POST con el name de nombre
if (isset($_POST["nombre"])) {
    // Realizando requires necesarios
    require_once "../core/DataSource.php";
    require_once "../model/Producto.php";
    // Creando objeto DataSource para conectarse a la bd
    $conexion = new DataSource();
    // Creando objeto del tipo Producto y asignando valores a los atributos
    $producto = new Producto($conexion->conectar());
    $producto->nombre = $_POST["nombre"];
    $producto->marca = $_POST["marca"];
    // Guardando un nuevo producto
    $producto->save();
    // Indicando manejo de variables de sesion
    session_start();
    // Asignando arreglo de productos a la variable de sesion productos
    $_SESSION["productos"] = $producto->all();
}
// Redireccionando al index
header("Location:../index.php");