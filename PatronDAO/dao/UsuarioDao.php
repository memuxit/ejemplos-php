<?php
// Agregando scripts necesarios
require_once("IDao.php");
require_once("../ds/DataSource.php");
require_once("../dto/Usuario.php");

// DAO para la tabla usuarios
class UsuarioDao implements IDao
{
    // Implementando metodo READ
    public function mostrar()
    {
        // Codigo
    }

    // Implementando metodo INSERT
    public function agregar($objeto)
    {
        // Creando objeto del DataSource
        $conexion = new DataSource();
        // Conectando y comprobando conexion
        if (!$conexion->conectar()) {
            echo "La conexion fallo";
            exit;
        } else {
            // Variable que contendra objeto DTO pasado como parametro
            $usuario = $objeto;
            // Variable con llamada al procedimiento almacenado
            $sql = "CALL agregarUsuario(?, ?)";
            // Preparando sentencia y evaluando preparacion
            if ($stmt = $conexion->preparar($sql)) {
                // Asignando variables para enviar como parametros al SP
                $stmt->bind_param("ss", $nombre, $password);
                // Obteniendo valores del objeto y asignandolos a las variables
                $nombre = $usuario->usuario;
                // Encriptando el password del usuario
                $password = password_hash($usuario->password, PASSWORD_DEFAULT);
                // Ejecutando sentencia
                $stmt->execute();
                // Obteniendo cantidad de registros afectados
                $registros = $stmt->affected_rows;
                // Cerrando conexiones y liberando recursos
                $stmt->close();
                $conexion->desconectar();
                // Retornando cantidad de registros afectados
                return $registros;
            } else {
                // Cerrando conexion y liberando recursos
                $conexion->desconectar();
                echo "Ocurrio un error al llamar al PS";
                exit;
            }
        }
    }

    // Implementando metodo UPDATE
    public function modificar($objeto)
    {
        // Codigo
    }

    // Implementando metodo DELETE
    public function eliminar($objeto)
    {
        // Codigo
    }

    public function verificarUsuario($usuario)
    {
        // Creando objeto del DataSource
        $conexion = new DataSource();
        // Conectando y comprobando conexion
        if (!$conexion->conectar()) {
            echo "La conexion fallo";
            exit;
        } else {
            // Variable que contendra la respuesta de la verificacion
            $valido = false;
            // Variable con llamada al procedimiento almacenado
            $sql = "CALL verificarUsuario(?)";
            // Preparando sentencia y evaluando preparacion
            if ($stmt = $conexion->preparar($sql)) {
                // Asignando variables para enviar como parametros al SP
                $stmt->bind_param("s", $nombre);
                // Obteniendo valores del objeto y asignandolos a las variables
                $nombre = $usuario->usuario;
                // Ejecutando sentencia
                $stmt->execute();
                // Vinculando variables a los campos de la tabla
                $stmt->bind_result($password);
                // Evaluando existencia de registros e iterando cada uno
                while ($stmt->fetch()) {
                    //  Evaluando si el password devuelto es igual al que contiene el DTO
                    if (password_verify($usuario->password, $password)) {
                        $valido = true;
                    }
                }
                // Cerrando conexiones y liberando recursos
                $stmt->close();
                $conexion->desconectar();
                // Retornando arreglo con los empleados
                return $valido;
            } else {
                // Cerrando conexiones y liberando recursos
                $conexion->desconectar();
                echo "Ocurrio un error al llamar al PS";
                exit;
            }
        }
    }
}
