<?php
// Obteniendo ruta del fichero a subir al servidor
$rutaArchivo = "img/".basename($_FILES["userfile"]["name"]);
// Obteniendo el tipo de fichero
$tipoArchivo = $_FILES["userfile"]["type"];
// Obteniendo tamanio del fichero
$tamanioArchivo = $_FILES["userfile"]["size"];
// Evaluando si el fichero no es gif, jpeg o si el tamanio es mayor a 100kb
if (!((strpos($tipoArchivo, "gif") || strpos($tipoArchivo, "jpeg")) && ($tamanioArchivo < 1000000))) {
    // Imprimiendo en caso de que no se cumplan las caracteristicas del fichero
    echo "Archivo incorrecto, vuelva a intentarlo";
} else {
    // Subiendo fichero y evaluando si se hizo correctamente
    if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $rutaArchivo)) {
        // Imprimiendo mensaje en el caso que se suba el fichero correctamente
        echo "La imagen ha sido cargada satisfactoriamente";
    } else {
        // Imprimiendo mensaje de error si no se pudo guardar el fichero
        echo "Hubo un error al subir la imagen";
    }
}
