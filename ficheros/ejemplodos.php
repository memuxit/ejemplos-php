<?php
// Abriendo documento de texto
$archivo = fopen("documento.txt", "r");
// Evaluando si existe el fichero
if ($archivo) {
    // Evaluando si no se ha llegado al final del documento
    while (!feof($archivo)) {
        // Imprimiendo parrafos del fichero
        echo fgets($archivo)."<br>";
    }
}