<?php

require_once "Producto.php";

/**
 * Clase que representa el modelo de la entidad Producto, heredando de la misma
 * 
 * @author Ing. Guillermo Rafael Vásquez Castaneda <memuxit@gmail.com>
 * @version 1.0
 */
class ProductosModel extends Producto
{
    /**
     * Atributo de la clase
     *
     * @var string
     */
    private $tabla;

    /**
     * Constructor de la clase que a su vez, llama al constructor padre
     *
     * @param PDO conexion con la base de datos
     */
    public function __construct($datasource)
    {
        $this->tabla = "productos";
        parent::__construct($datasource);
    }

    /**
     * Metodo que guarda un nuevo registro de la entidad Producto
     *
     * @return void
     */
    public function save()
    {
        $query = "INSERT INTO {$this->tabla} (nombre, marca) VALUES (:nombre, :marca)";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":nombre" => $this->nombre, ":marca" => $this->marca));
    }

    /**
     * Metodo que modifica un registro de la entidad Producto
     *
     * @return void
     */
    public function update()
    {
        $query = "UPDATE {$this->tabla} SET nombre = :nombre, marca = :marca WHERE id = :id";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":nombre" => $this->nombre, ":marca" => $this->marca, ":id" => $this->id));
    }

    /**
     * Metodo que devuelve un arreglo de registros de la entidad Producto
     *
     * @return array arreglo de objetos de la entidad Producto
     */
    public function all()
    {
        $productos = array();
        $query = "SELECT * FROM {$this->tabla}";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute();
        while ($producto = $stmt->fetch(PDO::FETCH_OBJ)) {
            array_push($productos, $producto);
        }
        return $productos;
    }

    /**
     * Metodo que elimina un registro de la entidad Producto
     *
     * @param int id de la entidad a eliminar
     * @return void
     */
    public function delete($id)
    {
        $query = "DELETE FROM {$this->tabla} WHERE id = :id";
        $stmt = $this->datasource->prepare($query);
        $stmt->execute(array(":id" => $id));
    }
}